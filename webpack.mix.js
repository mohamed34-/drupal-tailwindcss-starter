let mix = require('laravel-mix');
const CompressionPlugin = require('compression-webpack-plugin');
const zlib = require("zlib");
require('mix-tailwindcss');

// BrowserSync variables
const proxy = process.env.BS_DOMAIN || 'http://appserver_nginx';
const socketDomain = process.env.BS_SOCKET_DOMAIN || 'https://dev.drupal-tailwindcss-starter.lndo.site';
const socketPort = process.env.BS_SOCKET_PORT || 80;

mix.setPublicPath('dist')
  .js('src/js/app.js', 'app.js')
  .postCss("src/css/app.pcss", "app.css")
  .tailwind()
  .sass('src/sass/styles.scss', 'styles.css')
  .webpackConfig({
    devtool: 'eval-cheap-module-source-map',
    plugins: [
      new CompressionPlugin({
        filename: "[path][base].gz",
        algorithm: "gzip",
        test: /\.js$|\.css$|\.html$/,
        threshold: 10240,
        minRatio: 0.8,
      }),
      new CompressionPlugin({
        filename: "[path][base].br",
        algorithm: "brotliCompress",
        test: /\.(js|css|html|svg)$/,
        compressionOptions: {
          params: {
            [zlib.constants.BROTLI_PARAM_QUALITY]: 11,
          },
        },
        threshold: 10240,
        minRatio: 0.8,
      }),
  ]
  })
  .browserSync({
    proxy: proxy,
    ghostMode: false,
    socket: {
      domain: socketDomain,
      port: socketPort
    },
    files: [
      'dist/**/*',
      'templates/**/*',
      'src/**/*',
      'assets/**/*',
      'fonts/**/*',
      '../../../modules/custom/**/*',
    ],
    logLevel: "debug",
    logConnections: true,
    open: true
  })
  .extract()
  if (mix.inProduction()) {
    mix.version();
  }
